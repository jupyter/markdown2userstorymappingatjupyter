# Markdown2UserStoryMappingAtJupyter

This is a very basic repository resp. jupyter-notebook to demonstrate how to easily build a **UserStoryMapping** from a **markdown** document.

## How it works

This repository is a so called "Binder-ready-repository", as it contains all the necessary code and content as well a definitions of related dependencies.

All relevant informations on Jupyter & Binder, can be learned within the basic jupyter-tutorial ["Zero-to-Binder"](https://the-turing-way.netlify.app/communication/binder/zero-to-binder.html).

Inside the jupyter notebook we use the "cli-version" of the awesome tool [TextUSM](https://github.com/harehare/textusm), which does the real conversion between markdown and UserStoryMapping

A brief overview:

- _apt.txt_ → contains all the additionally needed system-packages (libraries)
- _postBuild_ → contains the installation of TextUSM-cli tool
- _main.ipynb_ → the actual jupyter notebook

More details can be found in the documentation of [repo2docker](https://repo2docker.readthedocs.io/en/latest/config_files.html)

## How to use it

### Online

You can use without having to install any local jupyter installation:

Simply copy the url of [this repo](https://gitlab.hrz.tu-chemnitz.de/jupyter/markdown2userstorymappingatjupyter.git), paste it to [mybinder](https://mybinder.org)...wait until it builds an interactive notebook for you...and start to play around!

[https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Fjupyter%2Fmarkdown2userstorymappingatjupyter.git/HEAD](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Fjupyter%2Fmarkdown2userstorymappingatjupyter.git/HEAD)

> **_NOTE:_**  Choose "Git repository" from the select!

### Locally

You can also use it with your local jupyter installation, but then you have to install the dependencies locally, which you can find in apt.txt & postBuild!

#### Set up virtual python environment

```bash
python3 -m venv /path/to/your/VirtEnvs/jupyter
. /path/to/your/VirtEnvs/jupyter/bin/activate
```

#### Integrate virtual node.js environment to python-venv

```bash
. /path/to/your/VirtEnvs/jupyter/bin/activate
pip install nodeenv
nodeenv -p
```

#### Install _textusm_

```bash
npm i -g textusm.cli
```

#### Install and start jupyter…

...**notebook** 

```bash
pip install jupyter
jupyter notebook --notebook-dir=/path/to/your/notebook
```

...**lab**

```bash
pip install jupyterlab
jupyter-lab --notebook-dir=/path/to/your/notebook
```

## How the conversion works

...is demonstrated interactively within the notebook!

## Credits

- [https://mybinder.org](https://mybinder.org)
- [https://textusm.com](https://textusm.com)
- [https://jupyter.org](https://jupyter.org)


